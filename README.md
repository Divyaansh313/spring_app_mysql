# WHAT WE BUILD

Created a Spring application connected to a MySQL Database (as opposed to an in-memory, embedded database, which most of the other guides and many sample applications use). It uses Spring Data JPA to access the database, but this is only one of many possible choices (for example, you could use plain Spring JDBC).

### Running the Spring Application

You can run the application from the command line with Gradle or Maven. You can also build a single executable JAR file that contains all the necessary dependencies, classes, and resources and run that. Building an executable jar makes it easy to ship, version, and deploy the service as an application throughout the development lifecycle, across different environments, and so forth.

If you use Gradle, you can run the application by using `./gradlew bootRun`. Alternatively, you can build the JAR file by using `./gradlew build` and then run the JAR file, as follows:

	java -jar build/libs/gs-accessing-data-mysql-0.1.0.jar

If you use Maven, you can run the application by using `./mvnw spring-boot:run`. Alternatively, you can build the JAR file with `./mvnw clean package` and then run the JAR file, as follows:

	java -jar target/gs-accessing-data-mysql-0.1.0.jar

When you run the application, logging output is displayed. The service should be up and running within a few seconds.

![Spring Application console output. It took a lot of time!](gs-accessing-data-mysql/Spring_console.png "Console Output")

### Test the Application

Now that the application is running, you can test it by using curl or some similar tool. You have two HTTP endpoints that you can test:

`GET localhost:8080/demo/all:` Gets all data.
`POST localhost:8080/demo/add:` Adds one user to the data.

The following curl command adds a user:

	$ curl localhost:8080/demo/add -d name=First -d email=someemail@someemailprovider.com

The reply should be as follows:

	Saved

The following command shows all the users:

	$ curl 'localhost:8080/demo/all'

The reply should be as follows:

	[{"id":1,"name":"First","email":"someemail@someemailprovider.com"}]

The output is shown below:

![Testing the Application console output. It took a lot of time!](gs-accessing-data-mysql/Data_Add_SQL.png "Console Output")

### SUMMARY

Just developed a Spring application that is bound to a MySQL database and is ready for production!

Connect me through LinkedIn:

<https://www.linkedin.com/in/divyaansh-jain-9b968a170/>
